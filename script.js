/*
## Задание 

Реализовать слайдер на чистом Javascript.

#### Технические требования:
- Создать HTML разметку, содержащую кнопки `Previous`, `Next`, и картинки (6 штук), которые будут пролистываться горизонтально. 
- На странице должна быть видна только одна картинка. Слева от нее будет кнопка `Previous`, справа - `Next`.
- По нажатию на кнопку `Next` - должна появиться новая картинка, следующая из списка.
- По нажатию на кнопку `Previous` - должна появиться предыдущая картинка.
- Слайдер должен быть бесконечным, т.е. если в начале нажать на кнопку `Previous`, то покажется последняя картинка, а если нажать на `Next`, когда видимая - последняя картинка, то будет видна первая картинка.
- Пример работы слайдера можно увидеть(http://kenwheeler.github.io/slick/) (первый пример).

*/

const slider = document.querySelector('.slider');
const btnPrev = document.querySelector('.btn-prev');
const btnNext = document.querySelector('.btn-next');
const imageslinks = [
    '<img class="img" src="img/img-1.jpg" alt="img">',
    '<img class="img" src="img/img-2.jpg" alt="img">',
    '<img class="img" src="img/img-3.jpg" alt="img">',
    '<img class="img" src="img/img-4.jpg" alt="img">',
    '<img class="img" src="img/img-5.jpg" alt="img">',
    '<img class="img" src="img/img-6.jpg" alt="img">'   
];

imgIndexCounter = 0;

btnNext.addEventListener('click', () => {
    getCurrentImgIndex(1);
    slider.innerHTML = `${imageslinks[imgIndexCounter]}`
});

btnPrev.addEventListener('click', () => {
    getCurrentImgIndex(-1);
    slider.innerHTML = `${imageslinks[imgIndexCounter]}`
});

function getCurrentImgIndex(n){
    let ansv = imgIndexCounter + n;
    if (ansv < 0){
        ansv = imageslinks.length-1;
    }
    if(ansv > imageslinks.length -1){
        ansv = 0;
    }
    imgIndexCounter = ansv;
}